﻿using System;
using System.Collections.Generic;
using Treehouse.Collections.Generic;
using Treehouse.Common;

namespace ExtensionMethods
{
    class Program
    {
        static void Main()
        {

            string myString = null;

            myString.IsNullOrEmpty();

            var synonymsForBest = new List<string>
            {
                "best",
                "greatest",
                "top",
                "leading",
                "most awesome",
                "unrivaled",
                "cheif",
                "imcomparable",
                "perfect",
                "preeminent",
                "premier",
                "ultimate",
                "excellent",
                "finest",
                "supreme",
            };
            Console.WriteLine($"My Dog Jojo is the {synonymsForBest.RandomItem()} dog!");
            Console.ReadLine();   
        } 
    }
}
