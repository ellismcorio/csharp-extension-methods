﻿using System;

namespace Treehouse.Common
{
    public static class StringExtensions
    {
        public static string[] Split(this string @this, char seperator, int count)
        {
            if (@this == null)
            {
                throw new ArgumentException(nameof(@this));
            }

            return @this.Split(new[] { seperator }, count);
        }

        public static bool IsNullOrEmpty(this string @this)
        {
            return string.IsNullOrEmpty(@this);
        }
    }
}

